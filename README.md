# mountainlab mountainsort resources for franklab

1. read this first! https://mountainsort.readthedocs.io/en/latest/index.html

2. then read this repo's wiki documentation for franklab specifics

3. clone this repo into ~/.mountainlab/packages/

4. create symlink so you can access these files in the mlpipeline GUI:

->cd ~

->ln -s .mountainlab/ mountainlab