# script to batch execute mountainsort calls across days and tetrodes, with sorting on segments (drift) and tagged curation. 
# currently, specify a list of tetrodes manually
# call from within the day's .mountain directory to sort that day's worth of data

# run the ntlinks call, scrape the tet info from the list of directories (or datasets.txt)

# just need to update the path to the pipeline below 
# to run:  .../.mountain$ python3 /path/to/batch_drifttag.py

import subprocess
from datetime import datetime
import numpy as np
import scipy.io as sp
import os, sys

#parent_path=os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
sys.path.append('/opt/mountainlab/packages/pyms')
from mlpy import DiskReadMda

#MS added section to get time_offsets from times.mat and total_duration for the day


nt_all=[14,16,17,18,19,20,5,7,8,9,10,11,12,13]  #all tets for hiro
day=17

#day's raw data directory
timesdir = '/mnt/WDexternal1/HiroData/hiro' + '{:02d}'.format(day)
dirstring = '/mnt/WDsilver1/HiroData/hiro' + '{:02d}'.format(day)

# Get time_offsets
times = sp.loadmat(timesdir+'/times.mat')
eptimes = times["ranges"][1:,:]
eptimes_samples = np.cumsum((np.diff(eptimes)/10000)*30000)

time_offsets = np.append(0,eptimes_samples[:-1])
time_offsets = ','.join(map(str,map(int,time_offsets)))

for nt_num in nt_all:
    
    # Get total mda duration - do per tet, since they can vary slightly based on the DSP
    mdapath = dirstring + '/hiro'+'{:02d}'.format(day) + '.mda/raw.nt' + '{:02d}'.format(nt_num) + '.mda'
    X=DiskReadMda(mdapath)
    M,N = X.N1(),X.N2()
    total_duration = str(N)

    # Now convert nt_num to string to call mountainsort
    nt_num='{:02d}'.format(nt_num)
    #nt_num=str(nt_num)
    print(['processing nt ' + nt_num])

# Perform the initial sort; generate firings_raw.mda. optional: save pre.mda, and filt.mda
# default params:   (specify below if you wish to change; this reflects params set in the pipeline ms3_taggedcuration)
# freq_min=600
# freq_max=6000
# freq_wid=1000
# whiten=true
# detect_threshold=3
# detect_sign=-1
# detect_interval=10
# clip_size=50
# adjacency radius=-1
# mask_out_artifacts=true
#'/home/mari/.mountainlab/packages/msdrift/pipelines/ms3_drifttag.mlp'
    subprocess.call(['mlp-run','/home/mari/.mountainlab/packages/msdrift/pipelines/ms3_drifttag.mlp','sort_on_segments', 
                     '--raw=nt' + nt_num + '/raw.mda.prv',
                     '--firings_out=nt' + nt_num + '/firings_raw.mda',
                     '--filt_out=nt' + nt_num + '/filt.mda',
                     '--pre_out=nt' + nt_num + '/pre.mda',
                     '--detect_sign=1',
                     '--samplerate=30000',
                     '--start_time=0',
                     '--time_offsets=' + time_offsets,
                     '--total_duration=' + total_duration])

# Calculate metrics on the raw clusters to generate metrics_raw.json
    subprocess.call(['mlp-run','/home/mari/.mountainlab/packages/msdrift/pipelines/ms3_drifttag.mlp','calc_metrics',
        	'--pre=nt' + nt_num + '/pre.mda',
        	'--firings=nt' + nt_num + '/firings_raw.mda',
        	'--samplerate=30000',
        	'--metrics=nt' + nt_num + '/metrics_raw.json'])

# Merge burst parents if you wish
        # subprocess.call(['mlp-run','/home/anna/Src/Matlab/trodesffcode/MSresources/ms3_taggedcuration.mlp','merge_burst_parents',
        #         '--metrics=nt' + nt_num + '/metrics_raw.json',
        #         '--firings=nt' + nt_num + '/firings_raw.mda',
        #         '--samplerate=30000',
        #         '--firings_merged=nt' + nt_num + '/firings_processed.mda'])

# IF you merged burst parents, recalculate metrics 
        # subprocess.call(['mlp-run','/home/anna/Src/Matlab/trodesffcode/MSresources/ms3_taggedcuration.mlp','calc_metrics',
        #         '--pre=nt' + nt_num + '/pre.mda',
        #         '--firings=nt' + nt_num + '/firings_processed.mda',
        #         '--samplerate=30000',
        #         '--metrics=nt' + nt_num + '/metrics_processed.json'])

# Tag clusters as rejected based on our curation criteria
# default params: (specify below if you wish to change, this reflects params set in ms3_taggedcuration.mlp)
# firing_rate_thresh=.01
# isolation_thresh=.50
# noise_overlap_thresh=.03
# peak_snr_thresh=1.5
    subprocess.call(['mlp-run','/home/mari/.mountainlab/packages/msdrift/pipelines/ms3_drifttag.mlp','add_tags',
                '--metrics=nt' + nt_num + '/metrics_raw.json',
                '--firing_rate_thresh=.01',
                '--isolation_thresh=.50',
                '--noise_overlap_thresh=.03',
                '--samplerate=30000',
                '--metrics_tagged=nt' + nt_num + '/metrics_processed.json'])

# Save all calls to this script into the ntdir so you have a record of all params (named with date and time)
    timestr=datetime.now().strftime('%Y%m%d_%H%M')
    subprocess.call(['cp', __file__,'nt' + nt_num + '/callscriptlog'+timestr+'.txt'])
