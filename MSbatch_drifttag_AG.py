# script to batch execute mountainsort calls across days and tetrodes 
# currently, specify a list of tetrodes manually
# call from within the day's .mountain directory to sort that day's worth of data
# in future, will be able to give a list of days and this script will go into each day,
# run the ntlinks call, scrape the tet info from the list of directories (or datasets.txt)

# tell ms where to find pyms/mlpy
import sys
sys.path.append('/opt/mountainlab/packages/pyms')

import subprocess, json
from datetime import datetime
from mlpy import DiskReadMda


#nt_all = [30]  # partial tets
#nt_all = [1, 2,3,4,5, 6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27, 28,29,30]  #all tets for gus
nt_all = [4,6,7,9,10,11,12,13,14,15,17,19,20,21,22,23,24,25,26,28,29,30]  #all tets for remy


for nt_num in nt_all:

        nt_num=str(nt_num)

        print(['processing nt ' + nt_num])

#read in the json list of eps to get start and total times
        json_path = 'nt' + nt_num + '/raw.mda.prv'
        with open(json_path, 'r') as f:
                ep_files = json.load(f)

        # initialize with 0 (first start time)
        lengths = [0]

        for idx in range(len(ep_files['files'])):
                ep_path=ep_files['files'][idx]['prv']['original_path']
                ep_mda=DiskReadMda(ep_path)
                #get length of the mda (N dimension)
                samplength = ep_mda.N2()
                #add to prior sum and append
                lengths.append(samplength + lengths[(idx)])

        #first entries (incl 0) are starttimes; last is total time
        tottime =str(lengths[-1])
        starttimes=str(lengths[0:-1])

# Perform the initial sort; generate firings_raw.mda. optional: save pre.mda, and filt.mda
# default params:   (specify below if you wish to change; this reflects params set in the pipeline ms3_drifttag.mlp)
# freq_min=600
# freq_max=6000
# freq_wid=1000
# whiten=true
# detect_threshold=3
# detect_sign=1 ****** NOTE this changed bc came from mari, so overwrite below
# detect_interval=10
# clip_size=50
# adjacency radius=-1
# mask_out_artifacts=true

# for drift tracking, break up day by ep boundaries (determined above)
# anneal afterwards and match clusters based on 100 last/100first 
# calculate metrics across whole day's worth of clusters
        subprocess.call(['mlp-run','/home/anna/bin/ms3_drifttag.mlp','sort_on_segments',
                     '--raw=nt' + nt_num + '/raw.mda.prv',
                     '--firings_out=nt' + nt_num + '/dfirings_raw.mda',
                     '--pre_out=nt' + nt_num + '/pre.mda',
                     '--_params=nt' + nt_num + '/params.json',
                     '--detect_sign=-1',
                     '--samplerate=30000',
                     '--time_offsets='+ starttimes[1:-1],
                     '--total_duration='+tottime])


# Calculate metrics on the raw clusters to generate metrics_raw.json
        subprocess.call(['mlp-run','/home/anna/bin/ms3_drifttag.mlp','calc_metrics',
        	'--pre=nt' + nt_num + '/pre.mda',
        	'--firings=nt' + nt_num + '/dfirings_raw.mda',
        	'--samplerate=30000',
        	'--metrics=nt' + nt_num + '/dmetrics_raw.json'])

# Merge burst parents if you wish
        # subprocess.call(['mlp-run','/home/anna/Src/Matlab/trodesffcode/MSresources/ms3_taggedcuration.mlp','merge_burst_parents',
        #         '--metrics=nt' + nt_num + '/metrics_raw.json',
        #         '--firings=nt' + nt_num + '/firings_raw.mda',
        #         '--samplerate=30000',
        #         '--firings_merged=nt' + nt_num + '/firings_processed.mda'])

# IF you merged burst parents, recalculate metrics 
        # subprocess.call(['mlp-run','/home/anna/Src/Matlab/trodesffcode/MSresources/ms3_taggedcuration.mlp','calc_metrics',
        #         '--pre=nt' + nt_num + '/pre.mda',
        #         '--firings=nt' + nt_num + '/firings_processed.mda',
        #         '--samplerate=30000',
        #         '--metrics=nt' + nt_num + '/metrics_processed.json'])

# Tag clusters as rejected based on our curation criteria
# default params: (specify below if you wish to change, this reflects params set in ms3_taggedcuration.mlp)
# firing_rate_thresh=.05
# isolation_thresh=.95
# noise_overlap_thresh=.03
# peak_snr_thresh=1.5
#                
        subprocess.call(['mlp-run','/home/anna/bin/ms3_drifttag.mlp','add_tags',
                '--metrics=nt' + nt_num + '/dmetrics_raw.json',
                '--samplerate=30000',
                '--metrics_tagged=nt' + nt_num + '/dmetrics_raw.json',
                '--firing_rate_thresh=.01'])

# Save all calls to this script into the ntdir so you have a record of all params (named with date and time)
        timestr=datetime.now().strftime('%Y%m%d_%H%M')
        subprocess.call(['cp', __file__,'nt' + nt_num + '/callscriptlog'+timestr+'.txt'])
