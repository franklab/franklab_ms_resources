# script to batch execute mountainsort calls across days and tetrodes 
# currently, specify a list of tetrodes manually
# call from within the day's .mountain directory to sort that day's worth of data
# in future, will be able to give a list of days and this script will go into each day,
# run the ntlinks call, scrape the tet info from the list of directories (or datasets.txt)

# just need to update the path to the pipeline below 
# to run:  /...20170811_remy.mountain$ python ~/Src/Matlab/trodesffcode/MSutils/MSbatch.py

import subprocess
from datetime import datetime

#check whether the .mountain directory exists. if not, run setup_NTlinks
#sbprocess.call(['/home/anna/Src/Matlab/trodesffcode/MSresources/setup_NTlinks.node.js',nameofmntdir])


#
#nt_all = [4]  # partial tets
nt_all = [2,3,4,6,7,9,10,11,12,13,14,15,16,17,19,20,21,22,23,24,25,26,28,29,30]  #all tets for remy


for nt_num in nt_all:

        nt_num=str(nt_num)

        print(['processing nt ' + nt_num])

# Perform the initial sort; generate firings_raw.mda. optional: save pre.mda, and filt.mda
# default params:   (specify below if you wish to change; this reflects params set in the pipeline ms3_taggedcuration)
# freq_min=600
# freq_max=6000
# freq_wid=1000
# whiten=true
# detect_threshold=3
# detect_sign=-1
# detect_interval=10
# clip_size=50
# adjacency radius=-1
#mask_out_artifacts=true
        subprocess.call(['mlp-run','/home/anna/Src/Matlab/trodesffcode/MSresources/ms3_drifttag.mlp','sort',
                '--raw=nt' + nt_num + '/raw.mda.prv',
         	'--firings_out=nt' + nt_num + '/firings_raw.mda',
         	'--pre_out=nt' + nt_num + '/pre.mda',
                '--filt_out=nt' + nt_num + '/filt.mda',
         	'--_params=nt' + nt_num + '/params.json',
                '--detect_sign=-1',
         	'--samplerate=30000'])

# Calculate metrics on the raw clusters to generate metrics_raw.json
        # subprocess.call(['mlp-run','/home/anna/Src/Matlab/trodesffcode/MSresources/ms3_taggedcuration.mlp','calc_metrics',
        # 	'--pre=nt' + nt_num + '/pre.mda',
        # 	'--firings=nt' + nt_num + '/firings_raw.mda',
        # 	'--samplerate=30000',
        # 	'--metrics=nt' + nt_num + '/metrics_raw.json'])

# Merge burst parents if you wish
        # subprocess.call(['mlp-run','/home/anna/Src/Matlab/trodesffcode/MSresources/ms3_taggedcuration.mlp','merge_burst_parents',
        #         '--metrics=nt' + nt_num + '/metrics_raw.json',
        #         '--firings=nt' + nt_num + '/firings_raw.mda',
        #         '--samplerate=30000',
        #         '--firings_merged=nt' + nt_num + '/firings_processed.mda'])

# IF you merged burst parents, recalculate metrics 
        # subprocess.call(['mlp-run','/home/anna/Src/Matlab/trodesffcode/MSresources/ms3_taggedcuration.mlp','calc_metrics',
        #         '--pre=nt' + nt_num + '/pre.mda',
        #         '--firings=nt' + nt_num + '/firings_processed.mda',
        #         '--samplerate=30000',
        #         '--metrics=nt' + nt_num + '/metrics_processed.json'])

# Tag clusters as rejected based on our curation criteria
# default params: (specify below if you wish to change, this reflects params set in ms3_taggedcuration.mlp)
# firing_rate_thresh=.05
# isolation_thresh=.95
# noise_overlap_thresh=.03
# peak_snr_thresh=1.5
#        subprocess.call(['mlp-run','/home/anna/Src/Matlab/trodesffcode/MSresources/ms3_taggedcuration.mlp','add_tags',
#                '--metrics=nt' + nt_num + '/metrics_raw.json',
#               '--samplerate=30000',
#                '--metrics_tagged=nt' + nt_num + '/metrics_processed.json'])

# Extract marks from using firings.mda spike times 
        subprocess.call(['mlp-run','/home/anna/.mountainlab/packages/franklab_ms_resources/MS_resources_dev/ms3_drifttag.mlp','py_extract_clips',
                '--timeseries=nt' + nt_num + '/filt.mda',
                '--firings=nt' + nt_num + '/firings_raw.mda',
                '--clips_out=nt' + nt_num + '/marks.mda',
                '--clip_size=1'])

# Save all calls to this script into the ntdir so you have a record of all params (named with date and time)
        timestr=datetime.now().strftime('%Y%m%d_%H%M')
        subprocess.call(['cp', __file__,'nt' + nt_num + '/callscriptlog'+timestr+'.txt'])
