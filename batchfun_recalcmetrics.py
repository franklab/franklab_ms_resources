# Mari Sosa: batch recalculate metrics, with tags, after mountainview merges
# This function takes a list of tetrodes as input and batch executes mountainsort3 calls across tetrodes. 
# Call from within the day's .mountain directory to sort that day's worth of data
# Remember to first run the ntlinks call.

# Update the path to your pipeline below. 
# Then, to run:  /...<date>_<animal>.mountain$ python /path/to/ms3batchfun.py 21 23 24
# where 21 23 24 are the list of ntrodes to sort, given as strings and separated by spaces.
# The list can be however long you want, but the format should match 
# your directory naming, so if directories are nt04 rather than nt4, 
# be sure to type 04 as the ntrode number.

import subprocess
import sys
import glob

def batchfun_recalcmetrics(nt_list):
    print('ntrodes given:',sys.argv[1:])

    for nt_num in nt_list:


        print(['re-calculating metrics for nt ' + nt_num])
        
        # If you made manual merges, re-calculate metrics on the raw clusters to generate metrics_processed.json
        subprocess.call(['mlp-run','/home/mari/.mountainlab/packages/msdrift/pipelines/ms3_drifttag.mlp','calc_metrics',
        	'--pre=nt' + nt_num + '/pre.mda',
        	'--firings=nt' + nt_num + '/firings.curated.mda',
        	'--samplerate=30000',
        	'--metrics=nt' + nt_num + '/metrics_processed.json'])

# Re-add tags for clusters as mua based on our curation criteria
# default params: (specify below if you wish to change, this reflects params set in ms3_taggedcuration.mlp)
# firing_rate_thresh=.01
# isolation_thresh=.95
# noise_overlap_thresh=.03
# peak_snr_thresh=1.5
        subprocess.call(['mlp-run','/home/mari/.mountainlab/packages/msdrift/pipelines/ms3_drifttag.mlp','add_tags',
                '--metrics=nt' + nt_num + '/metrics_processed.json',
                '--firing_rate_thresh=.01',
                '--isolation_thresh=.95',
                '--noise_overlap_thresh=.03',
                '--mv2file=' + glob.glob('nt' + nt_num + '/*.mv2')[0],
                '--samplerate=30000',
                '--metrics_tagged=nt' + nt_num + '/metrics_processed.json'])
                

if __name__ == '__main__':
    #Transfer command line arguments to py function input arguments
    batchfun_recalcmetrics(sys.argv[1:])
