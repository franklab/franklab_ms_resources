# Mari Sosa edited after AG original
# This function takes a list of tetrodes as input and batch executes mountainsort3 calls across tetrodes. 
# Call from within the day's .mountain directory to sort that day's worth of data
# Remember to first run the ntlinks call.

# Update the path to your pipeline below. 
# Then, to run:  /...<date>_<animal>.mountain$ python /path/to/ms3batchfun.py 21 23 24
# where 21 23 24 are the list of ntrodes to sort, given as strings and separated by spaces.
# The list can be however long you want, but the format should match 
# your directory naming, so if directories are nt04 rather than nt4, 
# be sure to type 04 as the ntrode number.

import subprocess
import sys

def ms3batchfun(nt_list):
    print('ntrodes given:',sys.argv[1:])

    for nt_num in nt_list:

        print(['processing nt ' + nt_num])

        # first perform the initial sort and curation
        # this saves curated clusters based on the original cluster metrics
        subprocess.call(['mlp-run','/home/mari/mountainlab3/pipelines/ms3_MS_hpc.mlp','sort',
        	'--raw=nt' + nt_num + '/raw.mda.prv',
        	'--firings_out=nt' + nt_num + '/firings_curated.mda',
		'--filt_out=nt' + nt_num + '/filt.mda',
        	'--pre_out=nt' + nt_num + '/pre.mda',
		'--firings_original_out=nt' + nt_num + '/firings_original.mda',
        	'--_params=nt' + nt_num + '/params.json',
        	'--samplerate=30000',
		'--cluster_metrics_out=nt' + nt_num + '/cluster_metrics_original.json'])
          # rerun metrics calculations post curation to update values and out put metrics_curated
          # also resaves curated clusters, doesnt change them
        subprocess.call(['mlp-run','/home/mari/mountainlab3/pipelines/ms3_MS_hpc.mlp','curate',
        	'--pre=nt' + nt_num + '/pre.mda',
        	'--firings=nt' + nt_num + '/firings_curated.mda',
        	'--samplerate=30000',
        	'--curated_firings=nt' + nt_num + '/firings_curated.mda',
        	'--cluster_metrics=nt' + nt_num + '/cluster_metrics_curated.json'])

if __name__ == '__main__':
    #Transfer command line arguments to py function input arguments
    ms3batchfun(sys.argv[1:])
