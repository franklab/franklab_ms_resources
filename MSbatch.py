# script to batch execute mountainsort calls across days and tetrodes 
# currently, specify a list of tetrodes manually
# call from within the day's .mountain directory to sort that day's worth of data
# in future, will be able to give a list of days and this script will go into each day,
# run the ntlinks call, scrape the tet info from the list of directories (or datasets.txt)

# just need to update the path to the pipeline below 
# to run:  /...20170811_remy.mountain$ python ~/Src/Matlab/trodesffcode/MSutils/MSbatch.py

import subprocess



#4,6,9,10,11,12,13,14,15,17,19,20,21,22,23,
nt_all = [24,25,26,28,29,30]

for nt_num in nt_all:

        nt_num=str(nt_num)

        print(['processing nt ' + nt_num])

# first perform the initial sort and curation
# this saves curated clusters based on the original cluster metrics
        subprocess.call(['mlp-run','/home/anna/Src/Matlab/trodesffcode/MSresources/ms3_AG.mlp','sort',
        	'--raw=nt' + nt_num + '/raw.mda.prv',
        	'--firings_out=nt' + nt_num + '/firings_curated.mda',
        	'--pre_out=nt' + nt_num + '/pre.mda',
        	'--detect_sign=-1',
        	'--_params=nt' + nt_num + '/params.json',
        	'--samplerate=30000'])
# rerun metrics calculations post curation to update values and out put metrics_curated
# also resaves curated clusters, doesnt change them
        subprocess.call(['mlp-run','/home/anna/Src/Matlab/trodesffcode/MSresources/ms3_AG.mlp','curate',
        	'--pre=nt' + nt_num + '/pre.mda',
        	'--firings=nt' + nt_num + '/firings_curated.mda',
        	'--samplerate=30000',
        	'--curated_firings=nt' + nt_num + '/firings_curated.mda',
        	'--metrics=nt' + nt_num + '/metrics_curated.json'])
